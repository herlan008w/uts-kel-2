const isPrime = require('./primeNumber');

describe('Feature: Checking if a number is prime', () => {
    describe('Scenario: Checking if a number is prime', () => {
        test('Given I have entered the number 2, 3, and 7, when I check if it is a prime number, then it should be true', () => {
            // Given
            const primeNumbers = [2, 3, 7];
            
            // When & Then
            primeNumbers.forEach(number => {
                expect(isPrime(number)).toBe(true);
            });
        });
    });

    describe('Scenario: Checking if a number is not prime', () => {
        test('Given I have entered the number 4 and 25, when I check if it is a prime number, then it should be false', () => {
            // Given
            const nonPrimeNumbers = [4, 25];

            // When & Then
            nonPrimeNumbers.forEach(number => {
                expect(isPrime(number)).toBe(false);
            });
        });
    });

    describe('Scenario: Checking if a number is not a number', () => {
        test("Given I have entered the number 'tiga', when I check if it is not a number, then it should say 'it's not a number'", () => {
            // Given
            const notNumber = 'tiga';

            // When
            const result = isPrime(notNumber);

            // Then
            expect(result).toBe("it's not a number");
        });
    });

    describe('Scenario: Performance test for isPrime function', () => {
        test('Given a large prime number, it should be computed efficiently', () => {
                // Given
            const largePrimeNumber = 9999991; // A large prime number
                
                // When
            const startTime = Date.now();
            const result = isPrime(largePrimeNumber);
            const endTime = Date.now();
            const elapsedTime = endTime - startTime;
    
                // Then
            expect(result).toBe(true);
                // Expecting the elapsed time to be less than 100 milliseconds
            expect(elapsedTime).toBeLessThan(100); 
        });
    });
});
