# Anggota Kelompok 2 

- Abdullah Hanif Arif (09012682327006)
- Herlan Wijaya (09012682327014)
- Novrinda Khairun Nisak (09012682327020)
- Nyimas Diah Permatasari (09012682327004)
- Suci Ramadini (09012682327012)
- Vira Putri Rahmadini (09012682327013)

# Automate Testing: Mengecek Bilangan Prima

Proyek ini bertujuan untuk melakukan automate testing pada fungsi-fungsi yang digunakan untuk mengecek apakah sebuah nilai adalah bilangan prima atau bukan. Proyek ini menggunakan Jest JS, Cucumber, dan Gherkin style dengan Node.js.

## Instalasi

1. Pastikan Anda memiliki Node.js terinstal di komputer Anda. Jika belum, Anda dapat mengunduhnya dari [situs web resmi Node.js](https://nodejs.org/).
2. Unduh atau clone repository ini ke dalam komputer Anda.
3. Buka terminal dan arahkan ke direktori proyek ini.
4. Jalankan perintah `npm install` untuk menginstal semua dependensi yang diperlukan.


## Cara Penggunaan

1. **Mengecek Bilangan Prima:**
   - Fungsi untuk mengecek apakah sebuah nilai adalah bilangan prima atau bukan terdapat dalam file `primeNumber.js`.
   - Skenario pengujian menggunakan Cucumber dituliskan dalam file `scenarios.feature`.
   - Implementasi step definitions untuk skenario pengujian ada di dalam file `primeNumber.test.js`.

2. **Menjalankan Pengujian:**
   - Untuk menjalankan pengujian menggunakan Jest, jalankan perintah `npm jest` di terminal.

3. **Scenarios :**
    - 
    Scenario: Checking if a number is prime
        Given I have entered the number 2, 3, and 7
        When I check if it is a prime number
        Then it should be true

    Scenario: Checking if a number is not prime
        Given I have entered the number 4 dan 25
        When I check if it is a prime number
        Then it should be false

    Scenario: Checking if a number
        Given I have entered the number 'tiga'
        When I check if it is not number
        Then it should say 'it's not a number'

    @performance
    Scenario: Computing efficiency of isPrime function with a large prime number
        Given a large prime number, it should be computed efficiently
        When the isPrime function is called with a large prime number
        Then the result should be true And the elapsed time should be less than 100 milliseconds

4. **Hasil :**
- ![alt text](image.png)


